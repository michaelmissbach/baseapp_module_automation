<?php

namespace HomeAutomation\HomeAutomationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DevicePass
 * @package HomeAutomation\HomeAutomationBundle\DependencyInjection\Compiler
 */
class DevicePass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {       

        $definition = $container->findDefinition('app_device_factory');

        $taggedServices = $container->findTaggedServiceIds('app.device');
        
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addDevice', array(new Reference($id)));
        }
    }
}
