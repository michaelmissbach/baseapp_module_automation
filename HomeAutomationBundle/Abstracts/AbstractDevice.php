<?php

namespace HomeAutomation\HomeAutomationBundle\Abstracts;

use Twig\Environment;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;

/**
 * Class AbstractDevice
 * @package HomeAutomation\HomeAutomationBundle\Abstracts
 */
abstract class AbstractDevice implements IDevice
{
    /**
     * @var Device
     */
    protected $nativeDevice;

    /**
     * @param Device $nativeDevice
     */
    public function injectNativeDevice(Device $nativeDevice)
    {
        $this->nativeDevice = $nativeDevice;
    }

    /**
     * @return Device
     */
    public function getNativeDevice(): Device
    {
        return $this->nativeDevice;
    }

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @param Environment $twig
     */
    public function injectTwig(Environment $twig): void
    {
        $this->twig = $twig;
    }

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @param array $configuration
     */
    public function injectConfiguration(array $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return array
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }    

    /**
     * @param string $templatePath
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(string $templatePath = 'devices/dummy.html.twig'): string
    {
        return $this->twig->render($templatePath,['device'=>$this]);
    }

    /**
     * @return []
     */
    public function getProgramConfigurationTemplate(): array
    {
        return [
            'command'=>''
        ];
    }
}
