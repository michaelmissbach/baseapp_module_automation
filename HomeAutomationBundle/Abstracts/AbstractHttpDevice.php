<?php

namespace HomeAutomation\HomeAutomationBundle\Abstracts;

use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Service\AlertService;
use HomeAutomation\HomeAutomationBundle\Exceptions\DeviceNotRespondingException;

/**
 * Class AbstractDevice
 * @package HomeAutomation\HomeAutomationBundle\Abstracts
 */
abstract class AbstractHttpDevice extends AbstractDevice
{
    /**
     * 
     */
    protected function executeHttpRequest($url,$timeout = 20)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $contentCode = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
         
        curl_close($ch);

        if ($response === false) {
            AlertService::$instance->log(Alert::TYPE_INFO,sprintf('Device is not responding (%s).',$url));
            throw new DeviceNotRespondingException();
        }

        if(strpos(strtolower($contentCode),"json") !== false) {
            $response = json_decode($response,true);
        }

        return $response;
    }
}
