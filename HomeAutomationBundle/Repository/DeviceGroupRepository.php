<?php

namespace HomeAutomation\HomeAutomationBundle\Repository;

use HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup;
use Doctrine\ORM\EntityRepository;

/**
 * Class DeviceGroupRepository
 * @package HomeAutomation\HomeAutomationBundle\Repository
 */
class DeviceGroupRepository extends EntityRepository
{
    public function findAll()
    {
        $qb = $this->_em->createQueryBuilder();
        return
            $qb
                ->select('d')
                ->from(DeviceGroup::class,'d')
                ->orderBy('d.sorting')
                ->getQuery()
                ->getResult();
    }
}
