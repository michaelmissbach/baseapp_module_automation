<?php

namespace HomeAutomation\HomeAutomationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Entity\SensorLog;

/**
 * Class SensorLogRepository
 * @package HomeAutomation\HomeAutomationBundle\Repository
 */
class SensorLogRepository extends EntityRepository
{
    public function filterByDay(Device $device, \DateTime $date)
    {
        $from = clone $date;
        $to = clone $date;

        $builder = $this->_em->createQueryBuilder();
        $builder
            ->select('s')
            ->from(SensorLog::class,'s')
            ->where('s.device = :device')
            ->andWhere('s.at > :from')
            ->andWhere('s.at < :to')
            ->orderBy('s.at', 'ASC')
            ->setParameter('device',$device)
            ->setParameter('from',$from->setTime(0,0,0))
            ->setParameter('to',$to->setTime(23,59,0))
            ;

        return $builder->getQuery()->getResult();
    }

    public function countByDevice(Device $device)
    {
        $builder = $this->_em->createQueryBuilder();
        $builder->select('count(s.id)')
            ->where('s.device = :device')
            ->from(SensorLog::class,'s')
            ->setParameter('device',$device)
            ;

        return $builder->getQuery()->getSingleScalarResult();
    }

    public function clearByDevice(Device $device)
    {
        $builder = $this->_em->createQueryBuilder();
        $builder
            ->delete(SensorLog::class,'s')
            ->where('s.device = :device')
            ->setParameter('device',$device);

        return $builder->getQuery()->getResult();
    }
}
