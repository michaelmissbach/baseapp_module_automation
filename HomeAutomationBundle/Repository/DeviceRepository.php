<?php

namespace HomeAutomation\HomeAutomationBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DeviceRepository
 * @package HomeAutomation\HomeAutomationBundle\Repository
 */
class DeviceRepository extends EntityRepository
{
}
