<?php

namespace HomeAutomation\HomeAutomationBundle\Interfaces;

interface IDeviceType
{    
    const TYPE_SWITCH = "switch";
    const TYPE_SENSOR = "sensor";
}
