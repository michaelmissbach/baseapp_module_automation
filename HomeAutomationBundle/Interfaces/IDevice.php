<?php

namespace HomeAutomation\HomeAutomationBundle\Interfaces;

/**
 * Interface IDevice
 * @package HomeAutomation\HomeAutomationBundle\Interfaces
 */
interface IDevice
{
    const CONTEXT_WEB = 'context_web';
    const CONTEXT_COMMAND = 'context_command';

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return []
     */
    public function getConfigurationTemplate(): array;

    /**
     * @return []
     */
    public function getProgramConfigurationTemplate(): array;

    /**
     * @param string $templatePath
     * @return string
     */
    public function render(string $templatePath = ''): string;

    /**
     * @param $params
     * @param string $context
     * @param bool $log
     * @return array
     */
    public function command($params,$context = IDevice::CONTEXT_WEB,$log = true): array;
}
