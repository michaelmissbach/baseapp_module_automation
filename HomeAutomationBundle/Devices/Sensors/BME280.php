<?php

namespace HomeAutomation\HomeAutomationBundle\Devices\Sensors;

use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDeviceType;
use HomeAutomation\HomeAutomationBundle\Abstracts\AbstractHttpDevice;
use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Service\AlertService;
use HomeAutomation\HomeAutomationBundle\Exceptions\DeviceNotRespondingException;

/**
 * Class BME280
 * @package HomeAutomation\HomeAutomationBundle\Devices\Sensors
 */
class BME280 extends AbstractHttpDevice
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'BME280';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return IDeviceType::TYPE_SENSOR;
    }

    /**
     * @return array
     */
    public function getConfigurationTemplate() :array
    {
        return [
            "ip"=>"",
            "temperature_min"=>"",
            "temperature_max"=>"",
            "temperature_step"=>"",
            "humidity_min"=>"",
            "humidity_max"=>"",
            "humidity_step"=>"",
            "pressure_min"=>"",
            "pressure_max"=>"",
            "pressure_step"=>"",
            "altitude_min"=>"",
            "altitude_max"=>"",
            "altitude_step"=>"",
            "temperature_offset"=>"",
            "humidity_offset"=>"",
            "pressure_offset"=>"",
            "altitude_offset"=>""
        ];
    }

    /**
     * @return array|string[]
     */
    public function getProgramConfigurationTemplate(): array
    {
        return [
            "command"=>"getState"
        ];
    }

    /**
     * @param string $templatePath
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(string $templatePath = 'devices/dummy.html.twig'): string
    {
        return parent::render('@HomeAutomation/devices/sensors/bme280/template.html.twig');
    }

    /**
     * @param $params
     * @param string $context
     * @param bool $log
     * @return array
     */
    public function command($params,$context = IDevice::CONTEXT_WEB,$log = true): array
    {
        $result = [];

        try {
            switch($params['command']) {
                case 'getState':
                    $url = sprintf('http://%s/get',$this->configuration['ip']);
                    $result['url'] = $url;
                    $result = $this->executeHttpRequest($url);
                    $result['temperature'] && isset($this->configuration['temperature_offset']) && $result['temperature']+=$this->configuration['temperature_offset'];
                    $result['humidity'] && isset($this->configuration['humidity_offset']) && $result['humidity']+=$this->configuration['humidity_offset'];
                    $result['pressure'] && isset($this->configuration['pressure_offset']) && $result['pressure']+=$this->configuration['pressure_offset'];
                    $result['altitude'] && isset($this->configuration['altitude_offset']) && $result['altitude']+=$this->configuration['altitude_offset'];
                    $result['result'] = $result;                    
                    break;                
                case 'reset':
                    $url = sprintf('http://%s/reset',$this->configuration['ip']);
                    $result['url'] = $url;
                    $result = $this->executeHttpRequest($url);
                    if ($context === IDevice::CONTEXT_COMMAND) {
                        AlertService::$instance->log(
                            Alert::TYPE_LOGINFO,
                            sprintf('"%s" resetted.',$this->nativeDevice->getDevice())
                        );
                    }
                    $result['result'] = $result;
                    break;
                default:
                    throw new \Exception(sprintf('Unknown command %s.',$params['command']));
            }
        }
        catch(DeviceNotRespondingException $e) {

            $result['device_not_responding'] = true;

        } catch (\Throwable | \Exception $e) {
            $result['error'] = true;
            $result['message'] = $e->getMessage();
            $result['file'] = $e->getFile();
            $result['line'] = $e->getLine();
        }

        return $result;
    }
}
