<?php

namespace HomeAutomation\HomeAutomationBundle\Devices;

use HomeAutomation\HomeAutomationBundle\Abstracts\AbstractDevice;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use Twig\Environment;

class DeviceFactory
{
    /**
     * @var array
     */
    protected $devices;

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * 
     */
    public function __construct(Environment $twig)
    {
        $this->devices = [];
        $this->twig = $twig;
    }

    /**
     * @param IDevice $device
     * @return void
     */
    public function addDevice(IDevice $device): void
    {
        $this->devices[$device->getName()] = 
        [
            'classname' => get_class($device),
            'name' => $device->getName(),
            'type' => $device->getType()
        ];
        
    }

    /**
     * @param string $name
     * @return AbstractDevice
     */
    public function getInstanceByName(string $name): AbstractDevice
    {
        $instanceName = $this->devices[$name]['classname'];


        /** @var AbstractDevice $instance */
        $instance = new $instanceName();
        $instance->injectTwig($this->twig);
        $instance->injectConfiguration($instance->getConfigurationTemplate());        

        return $instance;
    }

    /**
     * @param Device $device
     * @return AbstractDevice
     */
    public function getInstanceByDeviceEntity(Device $device): AbstractDevice
    {
        $instance = $this->getInstanceByName($device->getDevice());
        $instance->injectConfiguration($device->getConfiguration());
        $instance->injectNativeDevice($device);

        return $instance;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return array_keys($this->devices);
    }
}
