<?php

namespace HomeAutomation\HomeAutomationBundle\Devices\Switches;

use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDeviceType;
use HomeAutomation\HomeAutomationBundle\Abstracts\AbstractHttpDevice;
use BaseApp\BaseappBundle\Entity\Alert;
use BaseApp\BaseappBundle\Service\AlertService;
use HomeAutomation\HomeAutomationBundle\Exceptions\DeviceNotRespondingException;

/**
 * Class GenericHttp4WaySwitch
 * @package HomeAutomation\HomeAutomationBundle\Devices\Switches
 */
class GenericHttp4WaySwitch extends AbstractHttpDevice
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Generic http 4 way switch';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return IDeviceType::TYPE_SWITCH;
    }

    /**
     * @return array
     */
    public function getConfigurationTemplate() :array
    {
        return [
            "ip"=>"",
            "subid"=>"0|1|2|3",
            "hide_switch"=>"false|true",            
            "hide_impuls_switch"=>"false|true",
            "force_relay_switch"=>"false|true",
            "duration_default"=>"5"
        ];
    }

    /**
     * @return array|string[]
     */
    public function getProgramConfigurationTemplate(): array
    {
        return [
            "command"=>"getStates|switch|switch-impuls",
            "mode"=>"an|aus",
            "duration"=>"0",
            "rule"=>"",
            "waiting"=>""
        ];
    }

    /**
     * @param string $templatePath
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(string $templatePath = 'devices/dummy.html.twig'): string
    {
        return parent::render('@HomeAutomation/devices/switches/generichttp4wayswitch/template.html.twig');
    }

    /**
     * @param $params
     * @param string $context
     * @param bool $log
     * @return array
     */
    public function command($params,$context = IDevice::CONTEXT_WEB,$log = true): array
    {
        $result = [];

        try {
            switch($params['command']) {
                case 'getStates':
                    $url = sprintf('http://%s/status?target=%s',$this->configuration['ip'],$this->configuration['subid']);
                    $result['url'] = $url;
                    $result = $this->executeHttpRequest($url);
                    $result['result'] = $result;
                    break;
                case 'reset':
                    $url = sprintf('http://%s/reset',$this->configuration['ip']);
                    $result['url'] = $url;
                    $result = $this->executeHttpRequest($url);
                    if ($context === IDevice::CONTEXT_COMMAND) {
                        AlertService::$instance->log(
                            Alert::TYPE_LOGINFO,
                            sprintf('"%s" resetted.',$this->nativeDevice->getDevice())
                        );
                    }
                    $result['result'] = $result;
                    break;
                case 'switch':
                    $url = sprintf('http://%s/%s?target=%s',$this->configuration['ip'],$params['mode'],$this->configuration['subid']);
                    if(isset($this->configuration['force_relay_switch']) && $this->configuration['force_relay_switch']=='true') {
                        $url = sprintf('%s&force=true',$url);
                    }
                    $result['url'] = $url;
                    $result = $this->executeHttpRequest($url);
                    if ($context === IDevice::CONTEXT_COMMAND) {
                        if ($result['changed'] == 1 && $log) {
                            AlertService::$instance->log(
                                Alert::TYPE_LOGINFO,
                                sprintf('"%s" changed state to "%s".',$this->nativeDevice->getDeviceName(),$result['current'] == 1 ? 'on':'off')
                            );
                        }
                    }
                    $result['result'] = $result;
                    break;
                case 'switch-impuls':
                    $this->command(['command'=>'switch','mode'=>'an'],$context,$log);
                    sleep((int)$params['duration']);
                    $result = $this->command(['command'=>'switch','mode'=>'aus'],$context,$log);
                    break;
                default:
                    throw new \Exception(sprintf('Unknown command %s.',$params['command']));
            }
        }
        catch(DeviceNotRespondingException $e) {

            $result['device_not_responding'] = true;

        } catch (\Throwable | \Exception $e) {
            $result['error'] = true;
            $result['message'] = $e->getMessage();
            $result['file'] = $e->getFile();
            $result['line'] = $e->getLine();
        }

        return $result;
    }
}
