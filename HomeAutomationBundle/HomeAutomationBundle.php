<?php

namespace HomeAutomation\HomeAutomationBundle;

use HomeAutomation\HomeAutomationBundle\DependencyInjection\Compiler\DevicePass;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class HomeAutomationBundle
 * @package BaseApp\BaseappBundle
 */
class HomeAutomationBundle extends Bundle
{
    const MAJOR_VERSION = 1;
    const MINOR_VERSION = 8;
    const REVISION_VERSION = 1;

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(IDevice::class)
            ->addTag('app.device')
        ;

        $container->addCompilerPass(new DevicePass());

        parent::build($container);
    }
}
