<?php

namespace HomeAutomation\HomeAutomationBundle\Form;

use BaseApp\BaseappBundle\Entity\Groups;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * Class DeviceGroupFormType
 * @package HomeAutomation\HomeAutomationBundle\Form
 */
class DeviceGroupFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sorting', IntegerType::class, [
                'label' => 'Sorting',
                'empty_data' => '',
                'required' => false
            ])
            ->add('title', TextType::class, [
                'label' => 'Name',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('programActive', CheckboxType::class, [
                'label' => 'Program active',
                'required' => false,
            ])
            ->add('collapsed', CheckboxType::class, [
                'label' => 'Collapsed in dashboard',
                'required' => false,
            ])
            ->add('groups', EntityType::class, [
                'label'     => 'Groups',
                'expanded'  => true,
                'multiple'  => true,
                'class'        => Groups::class,
                'choice_label' => 'groupname',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => DeviceGroup::class,
                'attr' => [
                    'novalidate' => 'novalidate',
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'device_group_form';
    }
}