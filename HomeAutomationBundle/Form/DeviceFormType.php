<?php

namespace HomeAutomation\HomeAutomationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDeviceType;
use HomeAutomation\HomeAutomationBundle\Repository\DeviceRepository;

/**
 * Class DeviceFormType
 * @package HomeAutomation\HomeAutomationBundle\Form
 */
class DeviceFormType extends AbstractType
{
    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;
    
    /**
     * 
     */
    public function __construct(DeviceFactory $deviceFactory)
    {
        $this->deviceFactory = $deviceFactory;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $devicesList = [];        
        foreach ($this->deviceFactory->getList() as $entry) {
            $devicesList[$entry] = $entry;            
        }

        $builder
            ->add('deviceName', TextType::class, [
                'label' => 'Name',
                'empty_data' => '',
                'required' => false,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add(
                'deviceGroup',
                EntityType::class,
                [
                    'label' => 'Device group',
                    'choice_label' => 'title',
                    'class' => DeviceGroup::class,
                    'expanded' => false,
                    'multiple' => false,
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            )
            ->add('device', ChoiceType::class, [
                'label' => 'Typ',
                'empty_data' => '',
                'required' => false,
                'choices' => $devicesList,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('link', EntityType::class, [
                'label' => 'Linked Sensor',
                'empty_data' => '',
                'required' => false,
                'class' => Device::class,
                'query_builder' => function(DeviceRepository $repo) {
                    $results = $repo->findAll();
                    $tmp=[];
                    foreach ($results as $result) {
                        if ($result->getDevice() && $this->deviceFactory->getInstanceByDeviceEntity($result)->getType() === IDeviceType::TYPE_SENSOR) {
                            $tmp[] = $result->getId();
                        }
                    }
                    $qb = $repo->createQueryBuilder('entity');
                    $qb->where("entity.id IN(:ids)");
                    $qb->setParameter('ids',$tmp);
                    return $qb;                    
                }
            ])
            ->add('configuration', TextareaType::class, [
                'label' => 'Konfiguration',
                'empty_data' => '',
                'required' => false,
                 'attr' => [
                     'rows' => '8'
                ],
                 'constraints' => [
                     new NotBlank()
                 ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ])
        ;

        $builder->get('configuration')
            ->addModelTransformer(new CallbackTransformer(
                function ($configuration) {
                    $text = "";
                    foreach ($configuration as $key=>$value) {
                        $text .= sprintf('%s = %s%s',$key,$value,PHP_EOL);
                    }
                    return $text;
                },
                function ($configurationString) {
                    $configuration = [];
                    $splits = explode(PHP_EOL,$configurationString);
                    foreach ($splits as $split) {
                        $parts = explode(' = ',$split);
                        $key = $parts[0] ?? 'error';
                        $value = $parts[1] ?? '';
                        $configuration[$key]=str_replace(array("\r", "\n", "\t"), '', $value);
                    }
                    return $configuration;
                }
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Device::class,
                'attr' => [
                    'novalidate' => 'novalidate',
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'device_form';
    }
}