<?php

namespace HomeAutomation\HomeAutomationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use Symfony\Component\OptionsResolver\OptionsResolver;
use HomeAutomation\HomeAutomationBundle\Entity\Program;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;

/**
 * Class ProgramFormType
 * @package HomeAutomation\HomeAutomationBundle\Form
 */
class ProgramFormType extends AbstractType
{
    const SUNRISE_HOUR = 'sunrise_hour';
    
    const SUNRISE_MINUTE = 'sunrise_minute';
    
    const SUNSET_HOUR = 'sunset_hour';
    
    const SUNSET_MINUTE = 'sunset_minute';    

    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;
    
    /**
     * 
     */
    public function __construct(DeviceFactory $deviceFactory)
    {
        $this->deviceFactory = $deviceFactory;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $hours = [];
        $hours['Sunrise hour']= self::SUNRISE_HOUR;
        $hours['Sunset hour']=self::SUNSET_HOUR;
        for ($i=0;$i<24;$i++){
            if (strlen($i)<2) {
                $hours[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $hours[(string)$i] = (string)$i;
            }
        }

        $minutes = [];
        $minutes['Sunrise minute']= self::SUNRISE_MINUTE;
        $minutes['Sunset minute']=self::SUNSET_MINUTE;
        for ($i=0;$i<60;$i++){
            if (strlen($i)<2) {
                $minutes[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $minutes[(string)$i] = (string)$i;
            }
        }

        $builder
            ->add('device',EntityType::class, [
                'label' => 'Device',
                'empty_data' => '',
                'choice_label' => 'deviceName',
                'class' => Device::class,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Choose an device',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Active',
                'required' => false,
            ])
            ->add('startHour', ChoiceType::class, [
                'label' => 'Start hour',
                'empty_data' => '',
                'required' => false,
                'choices' => $hours,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('startMinute', ChoiceType::class, [
                'label' => 'Start minute',
                'empty_data' => '',
                'required' => false,
                'choices' => $minutes,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('startOffset', TextType::class, [
                'label' => 'Start offset minutes',
                'empty_data' => '',
            ])
            ->add('endHour', ChoiceType::class, [
                'label' => 'End hour',
                'empty_data' => '',
                'required' => false,
                'choices' => $hours,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('endMinute', ChoiceType::class, [
                'label' => 'End minute',
                'empty_data' => '',
                'required' => false,
                'choices' => $minutes,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('endOffset', TextType::class, [
                'label' => 'End offset minutes',
                'empty_data' => '',
            ])
            ->add('days', ChoiceType::class, [
                'label' => 'Weekdays',
                'empty_data' => '',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'choices' => [
                    'Monday' => 'Monday',
                    'Tuesday' => 'Tuesday',
                    'Wednesday' => 'Wednesday',
                    'Thursday' => 'Thursday',
                    'Friday' => 'Friday',
                    'Saturday' => 'Saturday',
                    'Sunday' => 'Sunday'
                ]
            ])
            ->add('configuration', TextareaType::class, [
                'label' => 'Konfiguration',
                'empty_data' => '',
                'required' => false,
                 'attr' => array('rows' => '8'),
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save'
            ])
        ;

        $builder->get('configuration')
            ->addModelTransformer(new CallbackTransformer(
                function ($configuration) {
                    $text = "";
                    foreach ($configuration as $key=>$value) {
                        $text .= sprintf('%s = %s%s',$key,$value,PHP_EOL);
                    }
                    return $text;
                },
                function ($configurationString) {
                    $configuration = [];
                    $splits = explode(PHP_EOL,$configurationString);
                    foreach ($splits as $split) {
                        $parts = explode(' = ',$split);
                        $key = $parts[0] ?? 'error';
                        $value = $parts[1] ?? '';
                        $configuration[$key]=str_replace(array("\r", "\n", "\t"), '', $value);
                    }
                    return $configuration;
                }
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Program::class,
                'attr' => [
                    'novalidate' => 'novalidate',
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'program_form';
    }
}