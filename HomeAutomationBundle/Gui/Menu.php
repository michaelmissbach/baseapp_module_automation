<?php

namespace HomeAutomation\HomeAutomationBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntryWithChilds;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuHeadline;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level2MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;

/**
 * Class Menu
 * @package HomeAutomation\HomeAutomationBundle\Gui
 */
class Menu implements IGui
{
    /**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {
        $sidebar = $guiBuilder->getSidebar();
        $sidebar->add(Level1Divider::create());
        $sidebar->add(Level1MenuHeadline::create()->setTitle('Automation'));
        $sidebar->add(
            Level1MenuEntry::create()
                ->setTitle('Dashboard')->setIcon('fas fa-fw fa-tachometer-alt')->setRouteName('automation_index')
        );
        $container = $guiBuilder->createGuiContainer();
        $sidebar->add(
            Level1MenuEntryWithChilds::create()
                ->setChildContainer($container)
                ->setTitle('Config')->setIcon('fas fa-fw fa-table')
        );
        $container->add(Level2MenuEntry::create()->setRouteName('devicegroups')->setTitle('Devicegroups'));
        $container->add(Level2MenuEntry::create()->setRouteName('devices')->setTitle('Devices'));
        $container->add(Level2MenuEntry::create()->setRouteName('programs')->setTitle('Programs'));
    }

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void
    {
        $guiBuilder->getHeadStyleSheets()->add(InternStyleSheet::create()->setTitle('/css/style.css'));
        $guiBuilder->getHeadJavascripts()->add(InternJavascript::create()->setTitle('/bundles/homeautomation/js/timevaluegraph.js'));
    }
}
