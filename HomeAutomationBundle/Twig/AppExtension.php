<?php

namespace HomeAutomation\HomeAutomationBundle\Twig;

use Twig\TwigFunction;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use Twig\Extension\AbstractExtension;

/**
 * Class AppExtension
 */
class AppExtension extends AbstractExtension
{   
    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;
    
    /**
     * 
     */
    public function __construct(DeviceFactory $deviceFactory)
    {
        $this->deviceFactory = $deviceFactory;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('deviceList', [$this, 'getDeviceList']),
            new TwigFunction('arrayParamsPrint', [$this, 'arrayParamsPrint']),
            new TwigFunction('arrayPrint', [$this, 'arrayPrint'])
        ];
    }

    /**
     * 
     */
    public function getDeviceList(): array
    {
        return $this->deviceFactory->getList();
    }

    /**
     * @param $configuration
     * @return string
     */
    public function arrayParamsPrint($configuration): string
    {
        $text = "";
        foreach ($configuration as $key=>$value) {
            if (strlen($text)) {
                $text .='<br>';
            }
            $text .= sprintf('%s = %s',$key,$value);
        }
        return $text;
    }

    /**
     * @param $configuration
     * @return string
     */
    public function arrayPrint($configuration): string
    {
        $text = "";
        foreach ($configuration as $value) {
            if (strlen($text)) {
                $text .= ', ';
            }
            $text .= $value;
        }
        return $text;
    }
}
