/**
  Steuerbefehl für Relais (Hex-Format):
  Relais 1 öffnen: A0 01 01 A2
  Relais 1 schließen: A0 01 00 A1
  Relais 2 öffnen: A0 02 01 A3
  Relais 2 schließen: A0 02 00 A2
  Relais 3 öffnen: A0 03 01 A4
  Relais 3 schließen: A0 03 00 A3
  Relais 4 öffnen: A0 04 01 A5
  Relais 4 schließen: A0 04 00 A4
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <iostream>
#include <string>

const char* MAJOR = "1";
const char* MINOR = "2";
const char* PATCH_LEVEL = "0";

const int maxInput = 4;
char* currentLine;
const char* ssid     = "DeicideNet1";
const char* password = "homeDeicidenet2904";

const byte close[4][4] = {{0xA0, 0x01, 0x01, 0xA2}, {0xA0, 0x02, 0x01, 0xA3}, {0xA0, 0x03, 0x01, 0xA4}, {0xA0, 0x04, 0x01, 0xA5}};
const byte open[4][4] = {{0xA0, 0x01, 0x00, 0xA1}, {0xA0, 0x02, 0x00, 0xA2}, {0xA0, 0x03, 0x00, 0xA3}, {0xA0, 0x04, 0x00, 0xA4}};
bool state[4] = {false, false, false, false};
bool restartForced = false;

String numberAsString[4] = {"0","1","2","3"};

ESP8266WebServer server(80);

void setup()
{
  Serial.begin(115200);
  Serial.println("Starten");

  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  server.on("/", printGui);
  server.on("/an", handleRequestOn);
  server.on("/aus", handleRequestOff);
  server.on("/impuls", handleRequestImpuls);
  server.on("/status", handleRequestStatus);
  server.on("/status_all", handleRequestStatusAll);
  server.on("/reset", handleRequestReset);
  server.on("/device", handleRequestDevice);

  server.begin();
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());

  resetStates();
}

void resetStates()
{
  for (int i = 0; i < maxInput; i++) {
    Serial.write(open[i], sizeof(open[i]));
    state[i] = false;
    delay(1000);
  }
}

void loop()
{
  server.handleClient();
  if (restartForced) {
    delay(5000);
    restartForced = false;
    resetStates();
    ESP.restart();
  }
}

void handleRequestOn()
{
  String message = "{";
  String number = server.arg("target");
  int i = number.toInt();
  if (state[i] == false || server.hasArg("force")) {
    Serial.write(close[i], sizeof(close[i]));
    state[i] = true;
    delay(10);
    message += "\"changed\":1,";
  } else {
    message += "\"changed\":0,";
  }

  message += "\"current\":";
  message += state[i] ? "1" : "0";
  message += "}";
  sendJson(message);
}

void handleRequestOff()
{
  String message = "{";
  String number = server.arg("target");
  int i = number.toInt();
  if (state[i] == true || server.hasArg("force")) {
    Serial.write(open[i], sizeof(open[i]));
    state[i] = false;
    delay(10);
    message += "\"changed\":1,";
  } else {
    message += "\"changed\":0,";
  }

  message += "\"current\":";
  message += state[i] ? "1" : "0";
  message += "}";
  sendJson(message);
}

void handleRequestImpuls()
{
  String number = server.arg("target");
  int i = number.toInt();
  String timespan = server.arg("timespan");
  int t = timespan.toInt();
  Serial.write(open[i], sizeof(open[i]));
  state[i] = false;
  delay(100);
  Serial.write(close[i], sizeof(close[i]));
  state[i] = true;
  delay(t);
  Serial.write(open[i], sizeof(open[i]));
  state[i] = false;
  delay(10);

  String message = "{\"done\":1}";
  sendJson(message);
}

void handleRequestStatus()
{
  String number = server.arg("target");
  int i = number.toInt();

  String message = "{";
  message += "\"current\":";
  message += state[i] ? "1" : "0";
  message += "}";
  sendJson(message);
}

void handleRequestStatusAll()
{
  String message = "{";
  for (int i = 0; i < maxInput; i++) {
    if (i > 0) {
      message += ",";
    }
    message += "\"current";
    message += numberAsString[i];
    message += "\":";
    message += state[i] ? "1" : "0";
  }

  message += "}";
  sendJson(message);
}

void handleRequestReset()
{
  String message = "{";
  message += "\"done\":1,";
  for (int i = 0; i < maxInput; i++) {
    if (i > 0) {
      message += ",";
    }
    message += "\"current";
    message += numberAsString[i];
    message += "\":";
    message += state[i] ? "1" : "0";
  }

  message += "}";
  sendJson(message);
  restartForced = true;
}

void handleRequestDevice()
{
  String message = "{\"type\":\"relais device\"}";
  sendJson(message);
}

void sendJson(String message)
{
  server.send(200, "application/json", message);
}

void printGui()
{
  String message = "<h1>Relaissteuerung</h1><br/>";
  message += "<br/>";
  message += "Version: ";
  message += MAJOR;
  message += ".";
  message += MINOR;
  message += ".";
  message += PATCH_LEVEL;

  server.send(200, "text/html", message);
}
