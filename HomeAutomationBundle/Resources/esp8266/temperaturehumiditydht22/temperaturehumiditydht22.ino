#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <iostream>
#include <string>
#include <DHT.h>
 
#define DHTTYPE   DHT22
#define DHTPIN    5

const char* MAJOR = "1";
const char* MINOR = "0";
const char* PATCH_LEVEL = "0";

const char* ssid     = "DeicideNet1";
const char* password = "homeDeicidenet2904";

DHT dht(DHTPIN, DHTTYPE, 22);
ESP8266WebServer server(80);

bool restartForced = false;
 
float temperature, humidity;
int soil = analogRead(A0);
float soil_percent;
unsigned long previousMillis = 0;
const long interval = 2000;

void setup()
{   
  Serial.begin(115200);
  Serial.println("Starten");

  dht.begin();
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  pinMode(soil, INPUT);

  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  server.on("/", printGui);
  server.on("/get", getState);
  server.on("/reset", handleRequestReset);
  
  server.begin();
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());   
}

void loop()
{
  server.handleClient();
  if (restartForced) {
    delay(5000);
    restartForced = false;
    ESP.restart();
  } 
}

void sendJson(String message)
{
  server.send(200, "application/json", message);
}

void getState()
{
  unsigned long currentMillis = millis();
 
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;

      humidity = dht.readHumidity();
      temperature = dht.readTemperature();
               

      if (isnan(humidity) || isnan(temperature)) {
          Serial.println("Failed to read from DHT sensor!");
          String message = "{"; 
          message += "}";  
          sendJson(message);
          return;          
      } else {
        Serial.println("Reporting " + String((int)temperature) + "C and " + String((int)humidity) + " % humidity");
        String message = "{";
        message += "\"temperature\":";
        message += String((int)temperature);
        message += ",";
        message += "\"humidity\":";
        message += String((int)humidity);  
        message += "}";
        
        sendJson(message);
        return;
      }
  String message = "{"; 
  message += "}";
  
  sendJson(message);   
      
  }

  String message = "{";
  message += "\"temperature\":";
  message += String((int)temperature);
  message += ",";
  message += "\"humidity\":";
  message += String((int)humidity);  
  message += "}";
  
  sendJson(message);
}

void handleRequestReset()
{
  String message = "{";
  message += "\"done\":1";
  message += "}";
  sendJson(message);
  restartForced = true;
}

void printGui()
{
  String message = "<h1>Temperatur Feuchtigkeitssensor DHT22</h1><br/>";
  message += "<br/>";
  message += "Version: ";
  message += MAJOR;
  message += ".";
  message += MINOR;
  message += ".";
  message += PATCH_LEVEL;

  server.send(200, "text/html", message);
}
