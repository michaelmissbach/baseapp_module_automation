 #include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <iostream>
#include <string>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

const char* MAJOR = "1";
const char* MINOR = "0";
const char* PATCH_LEVEL = "0";

const char* ssid     = "DeicideNet1";
const char* password = "homeDeicidenet2904";

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;

float temperature, humidity, pressure, altitude;

ESP8266WebServer server(80);

bool restartForced = false;
unsigned long previousMillis = 0;
const long interval = 2000;

void setup()
{   
  Serial.begin(115200);
  Serial.println("Starten");

  delay(100);
  
  bme.begin(0x76);   

  
  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  server.on("/", printGui);
  server.on("/get", getState);
  server.on("/reset", handleRequestReset);
  
  server.begin();
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());   
}

void loop()
{
  server.handleClient();
  if (restartForced) {
    delay(5000);
    restartForced = false;
    ESP.restart();
  } 
}

void sendJson(String message)
{
  server.send(200, "application/json", message);
}

void getState()
{ 
  
  unsigned long currentMillis = millis();
 
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;

      temperature = bme.readTemperature();
      humidity = bme.readHumidity();
      pressure = bme.readPressure() / 100.0F;
      altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);      
        
      String message = "{";
      message += "\"temperature\":";
      message += String((int)temperature);
      message += ",";
      message += "\"humidity\":";
      message += String((int)humidity);
      message += ",";
      message += "\"pressure\":";
      message += String((int)pressure);
      message += ",";
      message += "\"altitude\":";
      message += String((int)altitude);  
      message += "}";
      
      sendJson(message);
      return;     
  }

  String message = "{";
  message += "}";
  
  sendJson(message);
}

void handleRequestReset()
{
  String message = "{";
  message += "\"done\":1";
  message += "}";
  sendJson(message);
  restartForced = true;
}

void printGui()
{
  String message = "<h1>Temperatur Feuchtigkeitssensor BME280</h1><br/>";
  message += "<br/>";
  message += "Version: ";
  message += MAJOR;
  message += ".";
  message += MINOR;
  message += ".";
  message += PATCH_LEVEL;

  server.send(200, "text/html", message);
}
