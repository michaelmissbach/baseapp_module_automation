#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <iostream>
#include <string> 

const char* MAJOR = "1";
const char* MINOR = "2";
const char* PATCH_LEVEL = "0";

char* currentLine;
const char* ssid     = "DeicideNet1";
const char* password = "homeDeicidenet2904";

const byte close[4] = {0xA0, 0x01, 0x01, 0xA2};
const byte open[4] = {0xA0, 0x01, 0x00, 0xA1};
bool state = false;
bool restartForced = false;

ESP8266WebServer server(80);

void setup()
{  
  Serial.begin(9600);
  Serial.println("Starten");
  
  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  server.on("/",printGui);
  server.on("/an",handleRequestOn);  
  server.on("/aus",handleRequestOff);
  server.on("/impuls",handleRequestImpuls);
  server.on("/status",handleRequestStatus);
  server.on("/reset",handleRequestReset);
  server.on("/device",handleRequestDevice);
  
  server.begin();
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());

  resetStates();
}

void resetStates()
{
  Serial.write(open, sizeof(open));
  state = false;
}

void loop()
{
  server.handleClient();
  if (restartForced) {
    delay(5000);
    restartForced = false;
    resetStates();
    ESP.restart();
  }
}

void handleRequestOn()
{
  String message = "{";
  if (state == false || server.hasArg("force")) {
    Serial.write(close, sizeof(close));
    state = true;
    delay(10); 
    message += "\"changed\":1,";
  } else {
      message += "\"changed\":0,";
  }

  message += "\"current\":";
  message += state ? "1":"0";
  message += "}";
  sendJson(message); 
}

void handleRequestOff()
{
  String message = "{";
  if (state == true || server.hasArg("force")) {
    Serial.write(open, sizeof(open));
    state = false;
    delay(10);
    message += "\"changed\":1,";
  } else {
      message += "\"changed\":0,";
  }

  message += "\"current\":";
  message += state ? "1":"0";
  message += "}";
  sendJson(message);    
}

void handleRequestImpuls()
{
  String timespan = server.arg("timespan");
  int t = timespan.toInt();  
  Serial.write(open, sizeof(open));
  state = false;
  delay(100);
  Serial.write(close, sizeof(close));
  state = true;
  delay(t);
  Serial.write(open, sizeof(open));
  state = false;
  delay(10);

  String message = "{\"done\":1}";
  sendJson(message);
}

void handleRequestStatus()
{
  String message = "{";
  message += "\"current\":";
  message += state ? "1":"0";
  message += "}";
  sendJson(message);    
}

void handleRequestReset()
{
  String message = "{";
  message += "\"done\":1,";
  message += "\"current\":";
  message += state ? "1":"0";
  message += "}";
  sendJson(message);
  restartForced = true;
}

void handleRequestDevice()
{
  String message = "{\"type\":\"relais device\"}";
  sendJson(message);   
}

void sendJson(String message)
{
  server.send(200,"application/json",message);
}

void printGui()
{
  String message = "<h1>Relaissteuerung</h1><br/>";
  message += "<br/>";
  message += "Version: ";
  message += MAJOR;
  message +=".";
  message += MINOR;
  message +=".";
  message += PATCH_LEVEL;
  
  server.send(200, "text/html", message);     
}
