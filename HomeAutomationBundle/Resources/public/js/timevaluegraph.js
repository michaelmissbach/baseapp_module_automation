const TimeValueGraph = {
    initialized: false,
    selector: null,
    datalist: null,
    dataselector: null,
    miny: null,
    maxy: null,
    steps: null,
    padding: null,  
    suffix: null,
    init: function(selector,datalist,dataselector,miny,maxy,steps,suffix) {
        let instance = Object.create(this);
        instance.initialized = true;
        instance.selector = selector;
        instance.dataselector = dataselector;
        instance.miny = miny;
        instance.maxy = maxy;
        instance.steps = steps;
        instance.suffix = suffix;
        instance.padding = {top: 20, left: 20, right:20, bottom: 20};
        instance.update(datalist);
        $('#sidebarToggle, #sidebarToggleTop').on('click',function () {
            window.setTimeout(function(){instance.draw();},500);
        });
        window.addEventListener('resize', function () {
            window.setTimeout(function(){instance.draw();},500);
        });
        window.addEventListener("orientationchange", function() {
            window.setTimeout(function(){instance.draw();},500);
        });
        return instance;
    },
    update: function(datalist) {
        if (!this.initialized) {
            console.error('Instance not initialzed');
        }
        this.datalist = datalist;
        this.draw();
    },
    calculatePositionForTime: function(canvas, hour = 0, minute = 0, second = 0, padding) {
        let width_inner = canvas.width - padding.left - padding.right * 2;

        let smallest = width_inner / (24 * 60 * 60);
        let pos = 0;
        if (hour > 0) {
            pos = pos + (smallest * (hour * 60 * 60));
        }
        if (minute > 0) {
            pos = pos + (smallest * (minute * 60));
        }
        if (second > 0) {
            pos = pos + (smallest * second);
        }
        return parseInt(pos + padding.left);
    },
    calculatePositionForValue: function(canvas, value, padding) {
        let maxValue = this.maxy - this.miny;        
        let current = value - this.miny;
        let percentValue = (current * 100 / maxValue)/100;
        let height = canvas.height - padding.top - padding.bottom;
        let position = canvas.height - padding.bottom - (height * percentValue);
        return parseInt(position);
    },
    draw: function() {
        let canvas = $(this.selector+' canvas')[0];                
        canvas.width = $(this.selector).width();
        canvas.height = $(this.selector).height();
        
        let ctx = canvas.getContext('2d');
        ctx.translate(0.5,0.5);
        ctx.fillStyle = '#bac6c6';
        ctx.fillRect(0,0,canvas.width,canvas.height);

        this.drawGrid(canvas,ctx);
        this.drawData(canvas,ctx);
    },
    drawGrid: function(canvas,ctx) {
        let paddingTime = {left:this.padding.left +30, right: this.padding.right, top: this.padding.top, bottom: this.padding.bottom};
        ctx.fillStyle = '#001cc6';
        ctx.font = "10px Arial";
        for (var i = 0; i < 25; i++) {
            ctx.fillText(i, this.calculatePositionForTime(canvas,i,0,0,paddingTime) - (i > 9 ? 5 : 2), canvas.height - 10);
        }
        let paddingValue = {left:this.padding.left +30, right: this.padding.right, top: this.padding.top, bottom: this.padding.bottom + 5};
        ctx.fillStyle = '#001cc6';
        ctx.font = "10px Arial";
        
        ctx.fillText(this.miny+this.suffix, this.padding.left, this.calculatePositionForValue(canvas,this.miny,paddingValue) + 3);
        for (var i = this.miny+this.steps; i < this.maxy + this.steps; i+=this.steps) {
            let y = this.calculatePositionForValue(canvas,i,paddingValue);
            ctx.fillText(i+this.suffix, this.padding.left, y + 3);
            ctx.strokeStyle = '#867e84';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(this.padding.left + 30, y);
            ctx.lineTo(canvas.width - this.padding.right, y);
            ctx.stroke();
        }

        ctx.strokeStyle = '#867e84';
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(this.padding.left + 30 , this.padding.top );
        ctx.lineTo(this.padding.left + 30 , canvas.height - this.padding.bottom - 5 );
        ctx.lineTo(canvas.width - this.padding.right , canvas.height - this.padding.bottom - 5 );
        ctx.stroke();
    },
    drawData: function(canvas,ctx) {
        let paddingDot = {left:this.padding.left +30, right: this.padding.right, top: this.padding.top, bottom: this.padding.bottom + 5};
        ctx.fillStyle = "green";
        
        for (var i = 0; i < this.datalist.length; i++) {
            let value1 = this.datalist[i][this.dataselector];
            ctx.fillRect(
                this.calculatePositionForTime(canvas,this.datalist[i].hour,this.datalist[i].minute,0,paddingDot) - 1.5,
                this.calculatePositionForValue(canvas,value1,paddingDot) - 1.5,
                3,
                3
                );                
            try {
                let x1 = this.calculatePositionForTime(canvas,this.datalist[i].hour,this.datalist[i].minute,0,paddingDot);
                let y1 = this.calculatePositionForValue(canvas,value1,paddingDot);
                let value2 = this.datalist[i+1][this.dataselector];
                let x2 = this.calculatePositionForTime(canvas,this.datalist[i+1].hour,this.datalist[i+1].minute,0,paddingDot);
                let y2 = this.calculatePositionForValue(canvas,value2,paddingDot);
                ctx.beginPath();
                ctx.strokeStyle = '#FF0000';
                ctx.moveTo(x1 ,y1 );
                ctx.lineTo(x2 ,y2 );
                ctx.stroke();
            } catch(e) {}
        }
    }
};

