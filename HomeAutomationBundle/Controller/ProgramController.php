<?php

namespace HomeAutomation\HomeAutomationBundle\Controller;

use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Entity\Program;
use HomeAutomation\HomeAutomationBundle\Form\ProgramFormType;
use HomeAutomation\HomeAutomationBundle\Service\Calculation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ProgramController
 * @package HomeAutomation\HomeAutomationBundle\Controller
 */
class ProgramController  extends AbstractController
{
    const SESSION_DEVICE_FILTER = 'session_device_filter';

    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;

    /**
     * @var Calculation
     */
    protected $calculation;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * ProgramController constructor.
     * @param DeviceFactory $deviceFactory
     * @param Calculation $calculation
     * @param SessionInterface $session
     */
    public function __construct(DeviceFactory $deviceFactory, Calculation $calculation, SessionInterface $session)
    {
        $this->deviceFactory = $deviceFactory;
        $this->calculation = $calculation;
        $this->session = $session;
    }

    public function index()
    {
        $devices = $this->getDoctrine()->getRepository(Device::class)->findAll();
        foreach ($devices as $device) {
            $deviceList[$device->getDeviceName()] = $device->getDeviceName();
        }

        $selectedFilter = $this->session->get(self::SESSION_DEVICE_FILTER);
        if (strlen($selectedFilter)) {
            $qb = $this->getDoctrine()->getRepository(Program::class)->createQueryBuilder('entity');
            $programs =
                $qb->select('entity')
                    ->join('entity.device', 'device')
                    ->where(sprintf("device.deviceName = '%s'",$selectedFilter))
                    ->getQuery()
                    ->getResult();
        } else {
            $programs = $this->getDoctrine()->getRepository(Program::class)->findAll();
        }

        return $this->render('@HomeAutomation/program/index.html.twig', [
            'programs' => $programs,
            'deviceList' => $deviceList,
            'selectedFilter' => $selectedFilter
        ]);
    }

    public function delete($id)
    {
        $program = $this->getDoctrine()->getRepository(Program::class)->find($id);
        $this->getDoctrine()->getManager()->remove($program);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('programs'));
    }

    public function create()
    {
        $program = new Program();
        
        $selectedFilter = $this->session->get(self::SESSION_DEVICE_FILTER);
        if (strlen($selectedFilter)) {
            $device = $this->getDoctrine()->getRepository(Device::class)->findOneByDeviceName($selectedFilter);
            if ($device) {
                $program->setDevice($device);                
            }
        }

        $this->getDoctrine()->getManager()->persist($program);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('programs'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $program = $this->getDoctrine()->getRepository(Program::class)->find($id);

        $form = $this->createForm(ProgramFormType::class, $program);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Program $program */
            $program = $form->getData();
            $this->getDoctrine()->getManager()->persist($program);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('programs'));
        }

        return $this->render('@HomeAutomation/program/update.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param $params
     */
    public function setDeviceFilter($params): void
    {
        $this->session->set(self::SESSION_DEVICE_FILTER,$params['filter']);
    }

    public function getTime(): array
    {
        return ['time'=>(new \DateTime('now'))->format('H:i')];
    }

    /**
     * @param array $params
     * @return array
     */
    public function __getConfiguration($params)
    {
        $instance = $this->deviceFactory->getInstanceByName($params['type']);
        $configuration = $instance->getConfiguration();
        $text = "";
        foreach ($configuration as $key=>$value) {
            $text .= sprintf('%s = %s%s',$key,$value,PHP_EOL);
        }
        return ['content'=>$text];
    }
}