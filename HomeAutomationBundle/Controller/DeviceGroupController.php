<?php

namespace HomeAutomation\HomeAutomationBundle\Controller;

use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup;
use HomeAutomation\HomeAutomationBundle\Form\DeviceGroupFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DeviceGroupController
 * @package HomeAutomation\HomeAutomationBundle\Controller
 */
class DeviceGroupController  extends AbstractController
{
    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;   

    /**
     * 
     */
    public function __construct(DeviceFactory $deviceFactory)
    {
        $this->deviceFactory = $deviceFactory;
    }

    public function index()
    {
        $deviceGroups = $this->getDoctrine()->getRepository(DeviceGroup::class)->findAll();
        return $this->render('@HomeAutomation/devicegroup/index.html.twig', ['devicegroups' => $deviceGroups]);
    }

    public function delete($id)
    {
        $devicegroup = $this->getDoctrine()->getRepository(DeviceGroup::class)->find($id);
        $this->getDoctrine()->getManager()->remove($devicegroup);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('devicegroups'));
    }

    public function create()
    {
        $devicegroup = new DeviceGroup();
        $this->getDoctrine()->getManager()->persist($devicegroup);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('devicegroups'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $devicegroup = $this->getDoctrine()->getRepository(DeviceGroup::class)->find($id);

        $form = $this->createForm(DeviceGroupFormType::class, $devicegroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $devicegroup = $form->getData();
            $this->getDoctrine()->getManager()->persist($devicegroup);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('devicegroups'));

        }

        return $this->render('@HomeAutomation/devicegroup/update.html.twig', ['form' => $form->createView()]);
    }
}