<?php

namespace HomeAutomation\HomeAutomationBundle\Controller;

use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Entity\SensorLog;
use HomeAutomation\HomeAutomationBundle\Form\DeviceFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DeviceController
 * @package HomeAutomation\HomeAutomationBundle\Controller
 */
class DeviceController  extends AbstractController
{
    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;   

    /**
     * 
     */
    public function __construct(DeviceFactory $deviceFactory)
    {
        $this->deviceFactory = $deviceFactory;
    }

    public function index()
    {
        $devices = $this->getDoctrine()->getRepository(Device::class)->findAll();
        return $this->render('@HomeAutomation/device/index.html.twig', ['devices' => $devices]);
    }

    public function delete($id)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);
        $this->getDoctrine()->getManager()->remove($device);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('devices'));
    }

    public function create()
    {
        $device = new Device();
        $this->getDoctrine()->getManager()->persist($device);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('devices'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);

        $form = $this->createForm(DeviceFormType::class, $device);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $device = $form->getData();
            $this->getDoctrine()->getManager()->persist($device);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('devices'));

        }

        return $this->render('@HomeAutomation/device/update.html.twig', ['form' => $form->createView()]);
    }

    public function deleteLog(Request $request, $id)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find((int)$id);
        $this->getDoctrine()->getRepository(SensorLog::class)->clearByDevice($device);
        $this->addFlash('success','Log wurde gelöscht.');
        return $this->redirect($this->generateUrl('automation_index'));
    }


    /**
     * @param array $params
     * @return array
     */
    public function getConfiguration($params)
    {
        $instance = $this->deviceFactory->getInstanceByName($params['type']);
        $configuration = $instance->getConfiguration();       
        $text = "";
        foreach ($configuration as $key=>$value) {
            $text .= sprintf('%s = %s%s',$key,$value,PHP_EOL);
        }
        return ['content'=>$text];
    }

    /**
     * @param array $params
     * @return array
     */
    public function getProgramConfiguration($params)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($params['type']);
        $instance = $this->deviceFactory->getInstanceByName($device->getDevice());
        $configuration = $instance->getProgramConfigurationTemplate();
        $text = "";
        foreach ($configuration as $key=>$value) {
            $text .= sprintf('%s = %s%s',$key,$value,PHP_EOL);
        }
        return ['content'=>$text];
    }

    /**
     * @param array $params
     * @return array
     */
    public function commandToDeviceId($params)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($params['id']);
        $instance = $this->deviceFactory->getInstanceByDeviceEntity($device);
        $result = $instance->command($params);

        $device->setFeedback($result['result']);
        $this->getDoctrine()->getManager()->persist($device);
        $this->getDoctrine()->getManager()->flush();
        
        return $result;
    }

    public function getDeviceLog($params)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($params['id']);

        if (!$device) {
            return ['list'=>[],'logcount' => 0];
        }

        if (array_key_exists('date',$params)) {
            $date = new \DateTime($params['date']);
        } else {
            $date = new \DateTime();
        }

        $logs = $this->getDoctrine()->getRepository(SensorLog::class)->filterByDay($device,$date);

        $tmp = [];
        foreach ($logs as $log) {
            $obj = new \stdClass();
            $obj->hour = $log->getAt()->format('H');
            $obj->minute = $log->getAt()->format('i');
            foreach ($log->getPayload() as $key=>$value) {
                $obj->{$key} = $value;
            }
            $tmp[] = $obj;
        }

        return [
            'list'=>$tmp,
            'logcount' => $this->getDoctrine()->getRepository(SensorLog::class)->countByDevice($device)
        ];
    }
}
