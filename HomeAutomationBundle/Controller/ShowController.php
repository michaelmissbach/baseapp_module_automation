<?php

namespace HomeAutomation\HomeAutomationBundle\Controller;

use HomeAutomation\HomeAutomationBundle\Entity\Device;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use BaseApp\BaseappBundle\Entity\Groups;
use BaseApp\BaseappBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ShowController
 * @package HomeAutomation\HomeAutomationBundle\Controller
 */
class ShowController extends AbstractController
{
    /**
     * @param Request $request
     * @param DeviceFactory $factory
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, DeviceFactory $factory)
    {
        $deviceEntities = $this->getDoctrine()->getRepository(Device::class)->findAll();

        $devices = [];
        /** @var Device $device */
        foreach($deviceEntities as $device) {
            $group = $device->getDeviceGroup();

            if ($group) {
                $accessGroups = $group->getGroups();
                if (count($accessGroups)) {
                    $tmp = [];
                    /** @var Groups $singleAccessGroup */
                    foreach ($accessGroups as $singleAccessGroup) {
                        $tmp[] = $singleAccessGroup->getGroupname();
                    }
                    if (!UserService::$instance->isAllowed($tmp)) {
                        continue;
                    }
                }
            }

            if ($device->getDevice() && $group) {

                if (!isset($devices[$group->getSorting()])) {
                    $devices[$group->getSorting()]=[];
                }

                if (!array_key_exists($group->getTitle(),$devices[$group->getSorting()])) {
                    $devices[$group->getSorting()][$group->getTitle()] = [
                        'title' => $group->getTitle(),
                        'collapsed' => $group->isCollapsed(),
                        'active' => $group->getProgramActive(),
                        'list' => []
                    ];
                }
                $devices[$group->getSorting()][$group->getTitle()]['list'][] = $factory->getInstanceByDeviceEntity($device);
            }
        }

        ksort($devices);

        return $this->render('@HomeAutomation/show/main.html.twig',['devices'=>$devices]);
    }    
}
