<?php

namespace HomeAutomation\HomeAutomationBundle\Exceptions;

use Exception;

class DeviceNotRespondingException extends Exception
{    
}