<?php

namespace HomeAutomation\HomeAutomationBundle\Service;

use BaseApp\BaseappBundle\Service\SettingsService;
use HomeAutomation\HomeAutomationBundle\Form\ProgramFormType;

/**
 * Class Calculation
 * @package HomeAutomation\HomeAutomationBundle\Service
 */
class Calculation
{        
    /**
     * calcTimestamp
     *
     * @param  mixed $hour
     * @param  mixed $minute
     * @param  mixed $offsetMinutes
     * @return int
     */
    public function calcTimestamp($hour, $minute, $offsetMinutes = null): int
    {
        $seconds = 0;

        if ($hourValue = $this->calcSpecialValues($hour)) {
            if ($hourValue > 0) {
                $seconds += (int)$hourValue * 60 * 60;
            }                        
        }
        
        if ($hourMinute = $this->calcSpecialValues($minute)) {
            if ($hourMinute > 0) {
                $seconds += (int)$hourMinute * 60;
            }                        
        }

        if ($offsetMinutes) {
            $seconds = $seconds + (((int)$offsetMinutes) *  60);
       }

        return $seconds;
    }

    /**
     * @param [type] $value
     * @return integer|null
     */
    protected function calcSpecialValues($value): ?int
    {
        $returnValue = null;
        
        switch((string)$value) {
            case ProgramFormType::SUNRISE_HOUR:
                $returnValue = (int)$this->getSunriseAsArray()['hour'];
                break;
            case ProgramFormType::SUNRISE_MINUTE:
                $returnValue = (int)$this->getSunriseAsArray()['minute'];
                break;
            case ProgramFormType::SUNSET_HOUR:
                $returnValue = (int)$this->getSunsetAsArray()['hour'];
                break;
            case ProgramFormType::SUNSET_MINUTE:
                $returnValue = (int)$this->getSunsetAsArray()['minute'];
                break;
            default:
                $returnValue = (int)$value;
                break;
        }
        
        return $returnValue;
    }

    /**
     * @return array
     */
    protected function getSunriseAsArray(): array
    {        
        return $this->formatTimestampToHourAndMinute(
            date_sunrise(
                time(),
                SUNFUNCS_RET_TIMESTAMP,
                (float)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_latitude'),0),
                (float)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_longitude'),0),
                90
            )
        );            
    }
    
    /**
     * @return array
     */
    protected function getSunsetAsArray(): array
    {        
        return $this->formatTimestampToHourAndMinute(
            date_sunset(
                time(),
                SUNFUNCS_RET_TIMESTAMP,
                (float)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_latitude'),0),
                (float)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_longitude'),0),
                90
            )
        );            
    }

    /**
     * @param $timestamp
     * @return array
     */
    protected function formatTimestampToHourAndMinute($timestamp): array
    {
        $dateObject = (new \DateTime())->setTimestamp($timestamp);
        
        return [
            'hour' => $dateObject->format('H'),
            'minute' => $dateObject->format('i'),
        ];  
    }

    /**
     * @param $timestamp
     * @return array
     */
    public function calcTime($timestamp): array
    {
        return [
            'hour'   => ((int)($timestamp / 60 / 60)),
            'minute' => ($timestamp / 60 ) - ((int)($timestamp / 60 / 60 ) * 60)
        ];
    }
}
