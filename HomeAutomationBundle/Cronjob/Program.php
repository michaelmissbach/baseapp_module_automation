<?php

namespace HomeAutomation\HomeAutomationBundle\Cronjob;

use Doctrine\Persistence\ManagerRegistry;
use BaseApp\BaseappBundle\Cronjob\ICronjob;
use BaseApp\BaseappBundle\Service\StringHelper;
use Symfony\Component\HttpKernel\KernelInterface;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Service\SettingsService;
use Symfony\Component\HttpFoundation\ParameterBag;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use BaseApp\BaseappBundle\Interfaces\IAppSettingGroups;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use HomeAutomation\HomeAutomationBundle\Service\Calculation;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;
use HomeAutomation\HomeAutomationBundle\Entity\SensorLog;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDeviceType;

/**
 * Class Program
 * @package HomeAutomation\HomeAutomationBundle\Cronjob
 */
class Program implements ICronjob, IAppSettingGroups, IAppSettings
{
    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var Calculation
     */
    protected $calculation;
    
    /**
     * @var string[]
     */
    protected $weekdays = [
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
        7 => 'Sunday'
    ];

    /**
     * @var string
     */
    protected $cacheDir;

    /**
     * @var bool
     */
    protected $doLog = false;

    /**
     * Program constructor.
     * @param ManagerRegistry $doctrine
     * @param DeviceFactory $deviceFactory
     * @param Calculation $calculation
     * @param KernelInterface $kernel
     */
    public function __construct(
        ManagerRegistry $doctrine,
        DeviceFactory $deviceFactory,
        Calculation $calculation,
        KernelInterface $kernel
    )
    {
        $this->doctrine = $doctrine;
        $this->deviceFactory = $deviceFactory;
        $this->calculation = $calculation;

        $this->cacheDir = $kernel->getCacheDir();

        $this->expression = new ExpressionLanguage();
    }
    
    /**
     * getInterval
     *
     * @return int
     */
    public function getSecondInterval(): int
    {
        return (int)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_interval'),120);
    }

    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     */
    public function run(ParameterBag $request, ParameterBag $parameterBag): void
    {        
        $this->doLog = (bool)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','home_program_log'),false);

        $last = $this->getLastExecution($parameterBag);
        
        $message = sprintf('Last: %s:%s%s',$last['hour'],$last['minute'],PHP_EOL);
        $this->log($message,false);
        echo $message;

        $lastTimestamp = $this->calculation->calcTimestamp($last['hour'],$last['minute']);

        $now = new \DateTime('now');
        $nowTimestamp = $this->calculation->calcTimestamp($now->format('H'),$now->format('i'));
        $message = sprintf('Now: %s:%s%s',$now->format('H'),$now->format('i'),PHP_EOL);
        $this->log($message);
        echo $message;

        $fromDayToNextDay = false;
        if ($nowTimestamp < $lastTimestamp) {
            //set something greater then 23:59 :
            $nowTimestamp = $this->calculation->calcTimestamp(100,00);
            $fromDayToNextDay = true;
        }

        $this->loopOverSensors();

        $programs = $this->filterPrograms($lastTimestamp,$nowTimestamp);

        $this->loopOverPrograms($programs);

        if ($fromDayToNextDay) {
            $this->setLastExecution($parameterBag, 0,0);
            $message = sprintf('Set last execution to: %s:%s%s',0,0,PHP_EOL);
            $this->log($message);
            echo $message;
        } else {
            $now = $now->add(new \DateInterval('PT1M'));
            $this->setLastExecution($parameterBag, $now->format('H'),$now->format('i'));
            $message = sprintf('Set last execution to: %s:%s%s',$now->format('H'),$now->format('i'), PHP_EOL);
            $this->log($message);
            echo $message;
        }        
    }

    protected function loopOverSensors(): void
    {
        $devices = $this->doctrine->getRepository(Device::class)->findAll();

        foreach ($devices as $device) {
            /** @var IDevice $deviceInstance */
            $deviceInstance = $this->deviceFactory->getInstanceByDeviceEntity($device);            
            if ($deviceInstance->getType() !== IDeviceType::TYPE_SENSOR) {
                continue;
            }
            $result = $deviceInstance->command(['command'=>'getState'],IDevice::CONTEXT_COMMAND, $this->doLog);
            $device->setFeedback($result['result']);
            $sensorlog = new SensorLog();
            $sensorlog->setAt(new \DateTime);
            $sensorlog->setPayload($result['result']);
            $sensorlog->setDevice($device);
            $device->getSensorLogs()->add($sensorlog);
            $this->doctrine->getManager()->persist($device);
            $this->doctrine->getManager()->persist($sensorlog);
        }
        $this->doctrine->getManager()->flush();
    }

    /**
     * @param $programs
     */
    protected function loopOverPrograms($programs): void
    {
        foreach ($programs as $program) {

            $message = sprintf('Device "%s" command started.%s',$program->getDevice()->getDeviceName(),PHP_EOL);
            $this->log($message);
            echo $message;

            $device = $this->deviceFactory->getInstanceByDeviceEntity($program->getDevice());

            $config = $program->getConfiguration();
            foreach ($config as $key=>$value) {
                if (StringHelper::startsWith($config[$key],'expression:')) {
                    try {                    
                        $config[$key] = $this->expression->evaluate(
                            trim(substr($config[$key],11)),
                            [
                                'feedback' => $program->getDevice()->getLink() ? (object)$program->getDevice()->getLink()->getFeedback():'',
                            ]
                        );
                    } catch(\Exception | \Throwable $e) {
                        echo $e->getMessage();
                    }
                }
                
            }
            $result = $device->command($config,IDevice::CONTEXT_COMMAND, $this->doLog);

            $program->setLast(new \DateTime());
            $device->getNativeDevice()->setFeedback($result['result']);
            $this->doctrine->getManager()->persist($program);
            $this->doctrine->getManager()->persist($device->getNativeDevice());

            $message = sprintf('Result: %s%s',serialize($result),PHP_EOL);
            $this->log($message);
            echo $message;


            sleep(2);
        }
        $this->doctrine->getManager()->flush();
    }

    /**
     * @param $lastTimestamp
     * @param $nowTimestamp
     * @return array
     */
    protected function filterPrograms($lastTimestamp,$nowTimestamp): array
    {
        $weekday = (new \DateTime('now'))->format('N');

        $programs = [];
        /** @var HomeAutomation\HomeAutomationBundle\Entity\Program $program */
        foreach ($this->doctrine->getRepository(\HomeAutomation\HomeAutomationBundle\Entity\Program::class)
            ->findAll() as $program) {

            if (!$program->isActive()) {
                continue;
            }            
            if (
                self::checkRange(
                    $nowTimestamp,
                    $lastTimestamp,
                    $this->calculation->calcTimestamp($program->getStartHour(),$program->getStartMinute(),$program->getStartOffset()),
                    $this->calculation->calcTimestamp($program->getEndHour(),$program->getEndMinute(),$program->getEndOffset())
                    )
            ) {
                continue;
            }
            if (!in_array($this->weekdays[$weekday], $program->getDays())) {
                continue;
            }
            if (!$program->getDevice()->getDeviceGroup()->getProgramActive()) {
                continue;
            }            

            $configuration = $program->getConfiguration();
            if (array_key_exists('rule',$configuration) && strlen($configuration['rule']) > 0)
            {
                try {

                    $expressionResult = $this->expression->evaluate(
                        $configuration['rule'],
                        [
                            'feedback' => $program->getDevice()->getLink() ? (object)$program->getDevice()->getLink()->getFeedback():'',
                        ]
                    );
                    if (!$expressionResult) {
                        continue;
                    }

                } catch(\Exception | \Throwable $e) {}
            } 
            if (array_key_exists('waiting',$configuration) && (int)$configuration['waiting'] > 0)
            {
                if ($program->getLast() instanceof \DateTime) {
                    if ($program->getLast()->getTimestamp()+(int)$configuration['waiting'] > time()) {
                        continue;
                    }
                }
            }            

            $deviceName = $program->getDevice()->getDeviceName();
            if (array_key_exists($deviceName,$programs)) {

                if (
                    self::check(
                        $this->calculation->calcTimestamp($programs[$deviceName]->getStartHour(),$programs[$deviceName]->getStartMinute(),$programs[$deviceName]->getStartOffset()),
                        $this->calculation->calcTimestamp($programs[$deviceName]->getEndHour(),$programs[$deviceName]->getEndMinute(),$programs[$deviceName]->getEndOffset()),
                        $this->calculation->calcTimestamp($program->getStartHour(),$program->getStartMinute(),$program->getStartOffset()),
                        $this->calculation->calcTimestamp($program->getEndHour(),$program->getEndMinute(),$program->getEndOffset()),
                        $lastTimestamp,
                        $nowTimestamp)
                    ) {
                    $programs[$deviceName]=$program;                    
                }
                
            } else {
                $programs[$deviceName]=$program;
            }
        }

        return $programs;
    }

    /**
     * @param $nowTimestamp
     * @param $lastTimestamp
     * @param $start2
     * @param $end2
     * @return bool
     */
    public static function checkRange($nowTimestamp,$lastTimestamp,$start2,$end2): bool
    {
        return (!(((int)$lastTimestamp <= (int)$end2) and ($nowTimestamp >= $start2)));
        
    }

    /**
     * 
     * @param $start1
     * @param $end1
     * @param $start2
     * @param $end2
     * @param $nowTimestamp
     * @param $lastTimestamp
     * @return bool
     */
    public static function check($start1,$end1,$start2,$end2,$nowstart,$nowEnd): bool
    {
        if ($start2 > $start1) {          

            if ($end2 < $nowEnd) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param ParameterBag $parameterBag
     * @return array
     */
    protected function getLastExecution(ParameterBag $parameterBag): array
    {
        if (!$parameterBag->has('hour') || !$parameterBag->has('minute')) {
            $now = new \DateTime('now');
            $this->setLastExecution($parameterBag,$now->format('H'),$now->format('i'));
        }

        return [
            'hour' => $parameterBag->get('hour'),
            'minute' => $parameterBag->get('minute')
        ];
    }

    /**
     * @param ParameterBag $parameterBag
     * @param $hour
     * @param $minute
     */
    protected function setLastExecution(ParameterBag $parameterBag,$hour,$minute): void
    {
        $parameterBag->set('hour',$hour);
        $parameterBag->set('minute',$minute);
    }

    /**
     * @param $line
     * @param bool $append
     */
    protected function log($line,$append=true): void
    {
        $filename = $this->getLogExecutionFileName();

        if (!$append) {
            if (file_exists($filename)) {
                unlink($filename);
            }
        }

        if (is_array($line)) {
            $line = serialize($line);
        }

        if(!StringHelper::endsWith($line,PHP_EOL)) {
            $line = sprintf('%s%s', $line, PHP_EOL);
        }

        file_put_contents($filename, $line, FILE_APPEND);
    }

    /**
     * @return string
     */
    protected function getLogExecutionFileName(): string
    {
        return sprintf('%s/last_execution_log.txt',$this->cacheDir);
    }

    public function getAppSettingGroups(): array
    {
        return [
            [
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_GROUP_NAME => 'Home Automation'
            ]
        ];
    }

    public function getAppSettings(): array
    {
        return [
            [
                SettingsService::SETTINGS_KEY => 'home_program_interval',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Interval in seconds'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'user'
            ],
            [
                SettingsService::SETTINGS_KEY => 'home_program_log',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => CheckboxType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Log event active',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'home_program_latitude',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Latitude for sunrise/sunset calculation'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'home_program_longitude',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => TextType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Longitude for sunrise/sunset calculation'
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ]
        ];
    }
}
