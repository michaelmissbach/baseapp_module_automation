<?php

namespace HomeAutomation\HomeAutomationBundle\Cronjob;

use Doctrine\Persistence\ManagerRegistry;
use BaseApp\BaseappBundle\Cronjob\ICronjob;
use BaseApp\BaseappBundle\Interfaces\IMailSender;
use BaseApp\BaseappBundle\Interfaces\IAppSettings;
use BaseApp\BaseappBundle\Service\SettingsService;
use Symfony\Component\HttpFoundation\ParameterBag;
use HomeAutomation\HomeAutomationBundle\Entity\Device;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use HomeAutomation\HomeAutomationBundle\Interfaces\IDevice;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use HomeAutomation\HomeAutomationBundle\Devices\DeviceFactory;

/**
 * Class DeviceReset
 * @package BaseApp\BaseappBundle\Cronjob\BuiltIn
 */
class DeviceReset implements ICronjob, IAppSettings
{
   /**
     * @var DeviceFactory
     */
    protected $deviceFactory;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * DeviceReset constructor.
     * @param ManagerRegistry $managerRegistry
     * @param DeviceFactory $deviceFactory
     */
    public function __construct(ManagerRegistry $doctrine, DeviceFactory $deviceFactory)
    {
        $this->doctrine = $doctrine;
        $this->deviceFactory = $deviceFactory;
    }


    /**
     * @return array[]
     */
    public function getAppSettings(): array
    {
        $hours = [];
        for ($i=0;$i<24;$i++){
            if (strlen($i)<2) {
                $hours[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $hours[(string)$i] = (string)$i;
            }
        }

        $minutes = [];
        for ($i=0;$i<60;$i++){
            if (strlen($i)<2) {
                $minutes[sprintf('0%s',$i)] = sprintf('0%s',$i);
            } else {
                $minutes[(string)$i] = (string)$i;
            }
        }

        return [
            [
                SettingsService::SETTINGS_KEY => 'device_reset_active',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => CheckboxType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Devices daily reset',
                    'required' => false
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'device_reset_hour',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => ChoiceType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Devices daily reset hour',
                    'empty_data' => '',
                    'required' => false,
                    'choices' => $hours
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ],
            [
                SettingsService::SETTINGS_KEY => 'device_reset_minute',
                SettingsService::SETTINGS_GROUP_KEY => 'home_automation',
                SettingsService::SETTINGS_FORM_TYPE => ChoiceType::class,
                SettingsService::SETTINGS_FORM_TYPE_CONFIG => [
                    'label' => 'Devices daily reset minutue',
                    'empty_data' => '',
                    'required' => false,
                    'choices' => $minutes
                ],
                SettingsService::SETTINGS_USER_GROUP => 'admin'
            ]
        ];
    }

    /**
     * @return int
     */
    public function getSecondInterval(): int
    {
        return 60;
    }

    /**
     * @param ParameterBag $request
     * @param ParameterBag $parameterBag
     */
    public function run(ParameterBag $request, ParameterBag $parameterBag): void
    {
        $active = (bool)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','device_reset_active'),false);

        if (!$active) {
            return;
        }

        $hour = (int)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','device_reset_hour'),0);
        $minute = (int)SettingsService::$instance->get(SettingsService::createSemanticKey('home_automation','device_reset_minute'),0);
        
        $today = new \DateTime('now');
        $today->setTime($hour,$minute);

        if (!($today >= $request->get('last') && $today <= $request->get('now'))) {
            return;
        }
        
        $devices = $this->doctrine
            ->getRepository(Device::class)
            ->findAll();

        $list = [];     
        /** @var Device $device */   
        foreach ($this->doctrine->getRepository(Device::class)->findAll() as $device) {
            $config = $device->getConfiguration();
            if (isset($config['ip'])) {
                $list[$config['ip']] = $device;
            }
        }

        if (!count($list)) {
            return;
        }

        foreach ($list as $deviceNative) {

            $message = sprintf('Device "%s" reset.%s',$deviceNative->getDevice(),PHP_EOL);
            echo $message;

            $device = $this->deviceFactory->getInstanceByDeviceEntity($deviceNative);

            $result = $device->command(['command'=>'reset'],IDevice::CONTEXT_COMMAND, $this->doLog);

            $message = sprintf('Result: %s%s',serialize($result),PHP_EOL);
            echo $message;

            sleep(2);
        }
    }
}
