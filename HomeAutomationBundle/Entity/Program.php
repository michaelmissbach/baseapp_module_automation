<?php

namespace HomeAutomation\HomeAutomationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Program
 * @package HomeAutomation\HomeAutomationBundle\Entity
 * @ORM\Entity(repositoryClass="HomeAutomation\HomeAutomationBundle\Repository\ProgramRepository")
 */
class Program
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

	/**
     * @ORM\Column(type="string", length=255)
     */
	protected $startHour = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $startMinute = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $startOffset = '';


    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $endHour = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $endMinute = '';    

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $endOffset = '';

    /**
     * @ORM\Column(type="array")
     */
    protected $days = [];

	/**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

	/**
     * @ORM\Column(type="array")
     */
    protected $configuration = [];

    /**
     * One Product has One Shipment.
     * @ORM\ManyToOne(targetEntity="HomeAutomation\HomeAutomationBundle\Entity\Device")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     */
    protected $device;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $last;

    /**
     * Program constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Device
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartHour(): ?string
    {
        return $this->startHour;
    }

    /**
     * @param string $startHour
     * @return Program
     */
    public function setStartHour(?string $startHour): Program
    {
        $this->startHour = $startHour;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartMinute(): ?string
    {
        return $this->startMinute;
    }

    /**
     * @param string $startMinute
     * @return Program
     */
    public function setStartMinute(?string $startMinute): Program
    {
        $this->startMinute = $startMinute;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndHour(): ?string
    {
        return $this->endHour;
    }

    /**
     * @param string $endHour
     * @return Program
     */
    public function setEndHour(?string $endHour): Program
    {
        $this->endHour = $endHour;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndMinute(): ?string
    {
        return $this->endMinute;
    }

    /**
     * @param string $endMinute
     * @return Program
     */
    public function setEndMinute(?string $endMinute): Program
    {
        $this->endMinute = $endMinute;
        return $this;
    }

    /**
     * @return array
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * @param array $days
     * @return Program
     */
    public function setDays(array $days): Program
    {
        $this->days = $days;
        return $this;
    }

    /**
     * @return array
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * @param array $configuration
     * @return Program
     */
    public function setConfiguration(array $configuration): Program
    {
        $this->configuration = $configuration;
        return $this;
    }
    
    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param mixed $device
     * @return Program
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Program
     */
    public function setActive(bool $active): Program
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get the value of last
     */ 
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set the value of last
     *
     * @return  self
     */ 
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get the value of startOffset
     */ 
    public function getStartOffset()
    {
        return $this->startOffset;
    }

    /**
     * Set the value of startOffset
     *
     * @return  self
     */ 
    public function setStartOffset($startOffset)
    {
        $this->startOffset = $startOffset;

        return $this;
    }

    /**
     * Get the value of endOffset
     */ 
    public function getEndOffset()
    {
        return $this->endOffset;
    }

    /**
     * Set the value of endOffset
     *
     * @return  self
     */ 
    public function setEndOffset($endOffset)
    {
        $this->endOffset = $endOffset;

        return $this;
    }
}
