<?php

namespace HomeAutomation\HomeAutomationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SensorLog
 * @package HomeAutomation\HomeAutomationBundle\Entity
 * @ORM\Entity(repositoryClass="HomeAutomation\HomeAutomationBundle\Repository\SensorLogRepository")
 */
class SensorLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $at;
	
	/**
     * @ORM\Column(type="array")
     */
    protected $payload = [];
    
    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="sensorlogs")
     * @ORM\JoinColumn(name="sensorlog_id", referencedColumnName="id")
     */
    private $device;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of at
     */ 
    public function getAt()
    {
        return $this->at;
    }

    /**
     * Set the value of at
     *
     * @return  self
     */ 
    public function setAt($at)
    {
        $this->at = $at;

        return $this;
    }

	/**
	 * Get the value of payload
	 */ 
	public function getPayload()
	{
		return $this->payload;
	}

	/**
	 * Set the value of payload
	 *
	 * @return  self
	 */ 
	public function setPayload($payload)
	{
		$this->payload = $payload;

		return $this;
	}

    /**
     * Get many features have one product. This is the owning side.
     */ 
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set many features have one product. This is the owning side.
     *
     * @return  self
     */ 
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }
}
