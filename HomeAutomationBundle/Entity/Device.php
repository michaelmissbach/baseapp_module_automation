<?php

namespace HomeAutomation\HomeAutomationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Device
 * @package HomeAutomation\HomeAutomationBundle\Entity
 * @ORM\Entity(repositoryClass="HomeAutomation\HomeAutomationBundle\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="HomeAutomation\HomeAutomationBundle\Entity\DeviceGroup", inversedBy="devices")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     */
    protected $deviceGroup;

    /**
     * @ORM\Column(type="string", length=255)
     */
	protected $deviceName = '';

	/**
     * @ORM\Column(type="string", length=255)
     */
	protected $device = '';

	/**
     * @ORM\OneToMany(targetEntity="Device", mappedBy="link")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="children")
     * @ORM\JoinColumn(name="link_id", referencedColumnName="id")
     */
    protected $link;
	
	/**
     * @ORM\Column(type="array")
     */
	protected $configuration = [];
	
	/**
     * @ORM\Column(type="array",nullable=true)
     */
    protected $feedback = [];

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="SensorLog", mappedBy="device")
     */
    protected $sensorlogs;

    /**
     * Device constructor.
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sensorlogs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Device
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceName(): string
    {
        return $this->deviceName;
    }

    /**
     * @param string $deviceName
     */
    public function setDeviceName(string $deviceName): void
    {
        $this->deviceName = $deviceName;
    }

	/**
	 * Get the value of deviceGroup
	 *
	 * @return  DeviceGroup
	 */
	public function getDeviceGroup()
	{
		return $this->deviceGroup;
	}

	/**
	 * Set the value of deviceGroup
	 *
	 * @param   DeviceGroup  $deviceGroup
	 *
	 * @return  self
	 */
	public function setDeviceGroup(DeviceGroup $deviceGroup)
	{
		$this->deviceGroup = $deviceGroup;

		return $this;
	}	

	/**
	 * Get the value of configuration
	 *
	 * @return  array
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}

	/**
	 * Set the value of configuration
	 *
	 * @param   array  $configuration  
	 *
	 * @return  self
	 */
	public function setConfiguration(array $configuration)
	{
		$this->configuration = $configuration;

		return $this;
	}

	/**
	 * Get the value of device
	 *
	 * @return  mixed
	 */
	public function getDevice()
	{
		return $this->device;
	}

	/**
	 * Set the value of device
	 *
	 * @param   mixed  $device  
	 *
	 * @return  self
	 */
	public function setDevice($device)
	{
		$this->device = $device;

		return $this;
	}

    /**
     * Get the value of feedback
     */ 
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set the value of feedback
     *
     * @return  self
     */ 
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

	/**
	 * Get the value of Link
	 * 
	 * @return Device
	 */ 
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * Set the value of Link
	 *
	 * @return  self
	 */ 
	public function setLink(?Device $link)
	{
		$this->link = $link;

		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->deviceName;
	}

    /**
     * Get one Category has Many Categories.
     */ 
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set one Category has Many Categories.
     *
     * @return  self
     */ 
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get joinColumns={@ORM\JoinColumn(name="device_id", referencedColumnName="id")},
     */ 
    public function getSensorlogs()
    {
        return $this->sensorlogs;
    }

    /**
     * Set joinColumns={@ORM\JoinColumn(name="device_id", referencedColumnName="id")},
     *
     * @return  self
     */ 
    public function setSensorlogs($sensorlogs)
    {
        $this->sensorlogs = $sensorlogs;

        return $this;
    }
}
