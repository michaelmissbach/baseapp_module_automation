<?php

namespace HomeAutomation\HomeAutomationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DeviceGroup
 * @package HomeAutomation\HomeAutomationBundle\Entity
 * @ORM\Entity(repositoryClass="HomeAutomation\HomeAutomationBundle\Repository\DeviceGroupRepository")
 */
class DeviceGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="HomeAutomation\HomeAutomationBundle\Entity\Device", mappedBy="deviceGroup")
     */
    protected $devices;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $programActive = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $collapsed = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title = "";

    /**
     * @ORM\Column(type="integer")
     */
    protected $sorting = 1;

    /**
     * @ORM\ManyToMany(targetEntity="BaseApp\BaseappBundle\Entity\Groups")
     * @ORM\JoinTable(name="devicegroups_groups",
     *      joinColumns={@ORM\JoinColumn(name="devicegroup_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    protected $groups;

    /**
     * DeviceGroup constructor.
     */
    public function __construct()
    {
        $this->devices = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return DeviceGroup
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

	/**
	 * Get the value of devices
	 *
	 * @return  mixed
	 */
	public function getDevices()
	{
		return $this->devices;
	}

	/**
	 * Set the value of devices
	 *
	 * @param   mixed  $devices  
	 *
	 * @return  self
	 */
	public function setDevices($devices)
	{
		$this->devices = $devices;

		return $this;
	}

	/**
	 * Get the value of programActive
	 *
	 * @return  mixed
	 */
	public function getProgramActive()
	{
		return $this->programActive;
	}

	/**
	 * Set the value of programActive
	 *
	 * @param   mixed  $programActive  
	 *
	 * @return  self
	 */
	public function setProgramActive($programActive)
	{
		$this->programActive = $programActive;

		return $this;
	}

	/**
	 * Get the value of title
	 *
	 * @return  mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set the value of title
	 *
	 * @param   mixed  $title  
	 *
	 * @return  self
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

    /**
     * @return int
     */
    public function getSorting(): int
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     * @return DeviceGroup
     */
    public function setSorting(int $sorting): DeviceGroup
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     * @return DeviceGroup
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCollapsed(): bool
    {
        return $this->collapsed;
    }

    /**
     * @param bool $collapsed
     * @return DeviceGroup
     */
    public function setCollapsed(bool $collapsed): DeviceGroup
    {
        $this->collapsed = $collapsed;
        return $this;
    }
}
